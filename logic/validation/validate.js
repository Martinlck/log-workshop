/**
 * Created by martin on 31/03/15.
 */

var cfg = require('../../config')();


module.exports = function (matches) {
    if(matches == null || matches == undefined) {
        throw new Error("Matches are null, log format changed");
    }
    if(matches.length < cfg.total_matches) {
        throw new Error("Matches not properly sent");
    }
    var pathKeyValue = matches[cfg.path_index];

    var splitPathKeyValue = pathKeyValue.split("="); // this is already being validated by the regexp.

    if(splitPathKeyValue.length != 2 || splitPathKeyValue[1].indexOf("/") == -1) {
        throw new Error("Path matches not properly built");
    }

    var reg = new RegExp(/\/api\/users\/[0-9]+\/?/);
    if(reg.test(splitPathKeyValue[1])) {
        var pathArray = splitPathKeyValue[1].split("/");
        var verification = cfg[pathArray[pathArray.length  - 1]];
        if(verification !== undefined || !isNaN(pathArray[pathArray.length - 1])) {
            return true;
        }
    }
    return false;
}
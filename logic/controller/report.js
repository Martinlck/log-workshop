/**
 * Created by martin on 31/03/15.
 */

    var mathUtils = require('../../utils/mathUtils'),
        _         = require('underscore');

var keys = ["GET-/api/users/count_pending_messages", "POST-/api/users", "GET-/api/users/get_friends_progress", "GET-/api/users/get_friends_score", "GET-/api/users/get_messages", "GET-/api/users"]
var report = function() {
    this._init();
    this.dynoReport = {};
}

report.prototype._init = function() {
    var urlReport = {};
    keys.forEach(function (value) {
        urlReport[value] = {
            calls : 0,
            responseData : []
        };
    });
    this.urlReport = urlReport;
}

report.prototype.addToUrlReport = function (reportObject) {
    if(this.urlReport[reportObject.key] == undefined) {
        throw new Error("Report object key is not valid  key: " + reportObject.key);
    }
    this.urlReport[reportObject.key].calls += 1;
    this.urlReport[reportObject.key].responseData.push(reportObject.responseTime);
}

report.prototype.addToDynoReport = function (reportObject) {
    if(this.dynoReport[reportObject.dyno] == undefined) {
        this.dynoReport[reportObject.dyno] = 0;
    }

    this.dynoReport[reportObject.dyno] += 1;
}

report.prototype._getUrlReport = function (str) {
    for (var key in this.urlReport) {
        var average = mathUtils.getAverage(this.urlReport[key].responseData);
        var median  = mathUtils.getMedian(this.urlReport[key].responseData);
        var mode    = mathUtils.getMode(this.urlReport[key].responseData);
        str +=  "report for URL " + key + " is: \n - calls: " +  this.urlReport[key].calls + " - average response time : " + average + " - median response time : " + median + " - mode response time : " + mode+  "\n";
    }

    str += "\n\n";
    return str;
}

report.prototype._getDynoReport  = function (str) {
    var arrayOfDynos = [];
    _.each(this.dynoReport,function (value, key) {
        var sortedDyno = {};
        sortedDyno["key"] = key;
        sortedDyno["value"] = value;
        arrayOfDynos.push(sortedDyno);
    });

    var maxDyno = _.max(arrayOfDynos, function (object) {
        return object.value;
    })

    return "Max dyno is: " + maxDyno.key + " that has a value of: " + maxDyno.value + "\n\n";;
}

report.prototype.getReport = function () {
    var str = '';
    str += this._getUrlReport(str);
    str += this._getDynoReport(str);
    console.log(str);
}


module.exports = new report();



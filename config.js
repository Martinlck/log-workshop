/**
 * Created by martin on 31/03/15.
 */

module.exports = function () {
    return {
        log_file: "sample.log",
        total_matches : 10,
        path_index: 2,
        count_pending_messages : 1,
        get_messages : 1,
        get_friends_progress : 1,
        get_friends_score : 1
    }
}
/**
 * Created by martin on 31/03/15.
 */


var testlogic = require('./logic/testlogic'),
    cfg       = require('./config')(),
    ReportController = require('./logic/controller/report'),
    streamReader = require('./stream/streamReader');


streamReader(cfg.log_file, testlogic)(
    function () {
        ReportController.getReport()
    }
    , function (err) {
        console.log('Error while reading file.');
        console.log(err)
    }
);


/**
 * Created by martin on 31/03/15.
 */


var fs = require('fs'),
    es = require("event-stream");

module.exports = function (logFile, cb) {
    return function (cbOnEnd, cbOnError) {
        stream = fs.createReadStream(logFile).on('error', function (err) {
            cbOnError(err);
        }).pipe(es.split()).pipe(es.mapSync(
                function (line) {
                    stream.pause();
                    (function () {
                        cb(null, line, stream)
                    })();
                }).on('end', function () {
                    cbOnEnd();
                })
        );
    }
}


/**
 * Created by martin on 31/03/15.
 */


var mathUtils = module.exports;


mathUtils.getAverage = function (values) {
    if (values.length == 0)
        return null;
    var sum = 0;
    for (var i = 0; i < values.length; i++) {
        sum += parseInt(values[i], 10);
    }

    return sum / values.length;
}


mathUtils.getMedian = function (values) {
    if (values.length == 0)
        return null;
    values.sort(function (a, b) {
        return a - b;
    });

    var half = Math.floor(values.length / 2);

    if (values.length % 2)
        return values[half];
    else
        return (values[half - 1] + values[half]) / 2.0;
}


mathUtils.getMode = function (values) {
    if (values.length == 0)
        return null;

    var modeMap = {},
        maxEl = '',
        maxCount = 1;

    for(var i = 0; i < values.length; i++)
    {
        var el = values[i];

        if (modeMap[el] == null)
            modeMap[el] = 1;
        else
            modeMap[el]++;

        if (modeMap[el] > maxCount)
        {
            maxEl = el;
            maxCount = modeMap[el];
        }
        else if (modeMap[el] == maxCount)
        {
            maxEl += '&' + el;
            maxCount = modeMap[el];
        }
    }
    return maxEl;
}